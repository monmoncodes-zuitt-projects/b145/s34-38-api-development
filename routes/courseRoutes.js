const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

// ACTIVITY for API Development (Part 3)
// Add a course

router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	courseController
		.addCourse(req.body, userData.isAdmin)
		.then((resultFromController) => res.send(resultFromController));
});
module.exports = router;
