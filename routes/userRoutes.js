const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require("../auth");

// Checking Email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

// Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Login
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


//Activity#2
router.get("/details", (req, res) => {
	userController
		.getProfile(req.body.id)
		.then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
