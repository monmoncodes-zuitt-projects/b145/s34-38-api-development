const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const dotenv = require('dotenv');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require("./routes/courseRoutes");

//Environment Variables
dotenv.config();
const secret = (process.env.CONNECTION_STRING);

//Server Setup


const port = process.env.PORT || 4000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


//Middlewares
mongoose.connect(secret,
    {
        useNewUrlParser : true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection

    db.on('error', () => console.error.bind(console, 'error'))
    db.once('open', () => console.log('Successfully connected to MongoDB'))

app.use('/users', userRoutes);
app.use('/course', courseRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}`))
