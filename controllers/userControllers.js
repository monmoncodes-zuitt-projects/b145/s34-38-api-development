const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const router = require("../routes/userRoutes");

// Check if email exists

module.exports.checkEmailExists = (reqBody) => {
	
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){

			return true

		} else {
			 return false
		}
	})
}

// Registration

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err){

			 return false

		} else {

			return user
		}
	})
}

// login

module.exports.loginUser = (reqBody) => {

	return User.findOne({email : reqBody.email}).then(result => {

		if(result == null){

			return false
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password) 

			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result)}
			} else {

				return false
			}
		}
	})
}

//Activity#2
module.exports.getProfile = (userId) => {
	return User.findById(userId).then((result) => {
		result.password = "";
		return result;
	});
};
