const Course = require("../models/Course");
const router = require("../routes/courseRoutes");
const auth = require("../auth");

// ACTIVITY for API Development (Part 3)
// Add a course

module.exports.addCourse = (reqBody, isAdmin) => {
	if (isAdmin == true) {
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price,
		});
		return newCourse.save().then((course, err) => {
			if (err) {
				return false;
			} else {
				return course;
			}
		});
	}
};
